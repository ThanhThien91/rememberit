//
//  ConfigApp.h
//  AirPrint
//
//  Created by Thien Thanh on 7/7/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface ConfigApp : NSObject


/********************************************************************************/
#pragma mark - Database


#define kDatabaseName @"GameThuTriNho.sqlite"



/**************************************************************************''******/


/********************************************************************************/
#pragma mark - Time Play


#define kTimeSeeImage 5
#define kTimeAnswer 6


/********************************************************************************/


/********************************************************************************/
#pragma mark - Leaderboard


#define kLeaderboardIdentifier @"grp.com.carbon8.apps.gamecentertest"


/********************************************************************************/



/********************************************************************************/
#pragma mark - Ads

//  Ads
#define MOPUB_ADS_UNIT_BANNER @"ffd6c73ba50e42e5968b9f5d78935074"

/********************************************************************************/



/********************************************************************************/
#pragma mark - AppDelegate


+(AppDelegate *)getAppDelegate;



/********************************************************************************/
@end
