//
//  TutorialViewController.h
//  AirPrint
//
//  Created by Thien Thanh on 7/9/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewTutorial;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)action_Close:(id)sender;
@end
