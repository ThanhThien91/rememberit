//
//  TutorialViewController.m
//  AirPrint
//
//  Created by Thien Thanh on 7/9/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

/********************************************************************************/
#pragma mark  ViewDidLoad


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/** Register notification center for view controller */
-(void) registerNotification
{
    
}


/** Set data when view did load.
 ** Be there. You can set up some variables, data, or any thing that have reletive to data type*/
-(void) setDataWhenViewDidLoad
{
    
}


/** Set view when view did load
 ** Be there. You can change the layout, view, button,..*/
-(void) setViewWhenViewDidLoad
{
    [_scrollViewTutorial setContentSize:CGSizeMake(_scrollViewTutorial.frame.size.width * 2, _scrollViewTutorial.frame.size.height)];
    
}


/** Begin view controller */
- (void)viewDidLoad
{
    [self registerNotification];
    [self setViewWhenViewDidLoad];
    [self setDataWhenViewDidLoad];
    
    [super viewDidLoad];
}


-(void) viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

/********************************************************************************/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/********************************************************************************/
#pragma mark - ScrollView Delegate


- (void)scrollViewDidScroll:(UIScrollView *)sender {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = _scrollViewTutorial.frame.size.width;
    int page = floor((_scrollViewTutorial.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    _pageControl.currentPage = page;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark - Close View


- (IBAction)action_Close:(id)sender {
    [self.view removeFromSuperview];
}


/********************************************************************************/

@end
