//
//  QuestionViewController.m
//  AirPrint
//
//  Created by Thien Thanh on 7/7/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import "QuestionViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "GameOverViewController.h"
@import AVFoundation;
#import "TutorialViewController.h"

@interface QuestionViewController ()
{
    //  data
    NSMutableArray *listImageQuestion,*listQuestionOfImage;
    NSMutableArray *listImageQuestionAnswerTrue,*listQuestionOfImageAnswerTrue;
    
    //  Question
    ImageQuestionField *imageQuestionField;
    QuestionField *questionField;
    int score;
    
    //  timer
    NSTimer *timer;
    
    //  Image
    UIImage *imageNeedBlur;
    
    //  GameOver
    GameOverViewController *gameOver;
    
    //  Tutorial
    TutorialViewController *tutorialViewController;
    
    //  audio
    AVAudioPlayer *soundScoreUp,*soundFail,*soundBackground;

    
}

@property (nonatomic, retain) MPAdView *adView;

@end
\
@implementation QuestionViewController


/********************************************************************************/
#pragma mark  ViewDidLoad


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/** Register notification center for view controller */
-(void) registerNotification
{
    //  register notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playGameAgain) name:@"PlayGameAgain" object:nil];
    
    //  register notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToHomeScreen) name:@"HomeScreen" object:nil];
}


/** Set data when view did load.
 ** Be there. You can set up some variables, data, or any thing that have reletive to data type*/
-(void) setDataWhenViewDidLoad
{
    // Score
    score = 0;
    
    listImageQuestionAnswerTrue = [NSMutableArray new];
    listQuestionOfImageAnswerTrue = [NSMutableArray new];
    
    //  get list question
    ImageQuestionModel *imageQuestionModel = [ImageQuestionModel new];
    listImageQuestion = [imageQuestionModel getAllRowsInTableImageQuestion];
    
    //  show question
    [_timerAnwser setTranslatesAutoresizingMaskIntoConstraints:NO];
}


/** Set view when view did load
 ** Be there. You can change the layout, view, button,..*/
-(void) setViewWhenViewDidLoad
{
    //  ads
    [self loadMopubAds];
    
    //  sound
    [self initializeSound];
    [_btn_StartGame setHidden:NO];
    
    //  tutorial
    if (![ConfigApp getAppDelegate].isTutorial) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        tutorialViewController = [storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
        [self.view addSubview:tutorialViewController.view];
        [ConfigApp getAppDelegate].isTutorial = YES;
    }    
}


/** Begin view controller */
- (void)viewDidLoad
{
    
    [self registerNotification];
    [self setViewWhenViewDidLoad];
    [self setDataWhenViewDidLoad];
    
    [super viewDidLoad];
    
}


-(void) viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    //  remove notification
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

/********************************************************************************/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/********************************************************************************/
#pragma mark - Mopub Ads
-(void)loadMopubAds {
    self.adView = [[MPAdView alloc] initWithAdUnitId:MOPUB_ADS_UNIT_BANNER
                                                size:MOPUB_LEADERBOARD_SIZE];
    self.adView.delegate = self;
    CGRect frame = self.adView.frame;
    CGSize size = [self.adView adContentViewSize];
    frame.origin.y = [[UIScreen mainScreen] applicationFrame].size.height - size.height;
    self.adView.frame = frame;
    [self.view addSubview:self.adView];
    [self.adView loadAd];
}




/********************************************************************************/


#pragma mark - <MPAdViewDelegate>
- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}




/********************************************************************************/
#pragma mark - Select True or False



/**
 * Select False
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_SelectFalse:(id)sender {
    if ([listImageQuestion count] != 0)
    {
        if (questionField != nil) {
            if (questionField.answer == 0)
            {
                //  increase score
                [self increaseScore];
            }
            else
            {
                //  game over
                [self showScreenGameOver];
            }
        }
        
    }

}




/**
 * Select True
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_SelectTrue:(id)sender {
    
    if ([listImageQuestion count] != 0)
    {
        if (questionField != nil) {
            if (questionField.answer == 1)
            {
                //  increase score
                [self increaseScore];
            }
            else
            {
                //  game over
                [self showScreenGameOver];
            }
        }
        
    }
    
}



/**
 * Increase Score
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)increaseScore
{
    //  increase score
    score++;
    _lblScore.text = [NSString stringWithFormat:@"%i",score];
    
    [soundScoreUp play];
    
    //  next question
    [self nextQuestion];
}



/**
 * Next Question
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)nextQuestion
{
    //  empty question
    if ([listImageQuestion count] == 0) {
        return;
    }
    
    //  show image and hidden question
    [_lblQuestion setHidden:YES];
    
    //[_viewBlur setHidden:YES];
    
    //  show button select answer
    [_btnFalse setAlpha:0.5];
    [_btnFalse setUserInteractionEnabled:NO];
    [_btnTrue setAlpha:0.5];
    [_btnTrue setUserInteractionEnabled:NO];
    
    //  Image Question
    int random = arc4random() % [listImageQuestion count];
    imageQuestionField = listImageQuestion[random];
    [listImageQuestion removeObjectAtIndex:random];
    
    [listImageQuestionAnswerTrue addObject:imageQuestionField];
    [_imageQuestion setImage:nil];
    [_imageQuestion setImage:[UIImage imageNamed:imageQuestionField.nameImage]];
    
    
    //  timer
    [_timerAnwser setFrame:CGRectMake(0, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    [UIView animateWithDuration:kTimeSeeImage delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [_timerAnwser setFrame:CGRectMake(-_timerAnwser.frame.size.width, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    }completion:^(BOOL finished){
        //[self showQuestion];
    }];
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:kTimeSeeImage  target:self selector:@selector(showQuestion) userInfo:nil repeats:NO];
    
    //  blur image
    imageNeedBlur = [self blur:_imageQuestion.image];
    
}




/**
 * Show question
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)showQuestion
{
    //  hidden image and show question
    [_lblQuestion setHidden:NO];
    //[_viewBlur setHidden:NO];
    [_imageQuestion setImage:nil];
    [_imageQuestion setImage:imageNeedBlur];
    
    
    
    //  hide button select answer
    [_btnFalse setAlpha:1];
    [_btnFalse setUserInteractionEnabled:YES];
    [_btnTrue setAlpha:1];
    [_btnTrue setUserInteractionEnabled:YES];

    
    //  timer
    [_timerAnwser setFrame:CGRectMake(0, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    [UIView animateWithDuration:kTimeAnswer delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [_timerAnwser setFrame:CGRectMake(-_timerAnwser.frame.size.width, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    }completion:^(BOOL finished){
        
    }];
    [timer invalidate];
    if ([listImageQuestion count] != 0) {
        timer = [NSTimer scheduledTimerWithTimeInterval:kTimeAnswer  target:self selector:@selector(showScreenGameOver) userInfo:nil repeats:NO];
    }

    
    //  Question
    QuestionModel *questionModel = [QuestionModel new];
    listQuestionOfImage = [questionModel getRowByRowsInTableQuestionByIdImage:imageQuestionField.rowid];
    int random = arc4random() % [listQuestionOfImage count];
    questionField = listQuestionOfImage[random];
    [listQuestionOfImageAnswerTrue addObject:questionField];
    _lblQuestion.text = questionField.question;
    
    
    
    
}




/**
 * Game Over
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)showScreenGameOver
{
    //  fail
    [timer invalidate];
    [_timerAnwser setFrame:CGRectMake(0, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    [soundBackground stop];
    [soundFail play];
    
    //  Hidden button answer
    [_btnFalse setHidden:YES];
    [_btnTrue setHidden:YES];
    
    //  show game over
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    gameOver = [storyboard instantiateViewControllerWithIdentifier:@"GameOverViewController"];
    gameOver.score = score;
    gameOver.listQuestionOfImageAnswerTrue = listQuestionOfImageAnswerTrue;
    gameOver.listImageQuestionAnswerTrue = listImageQuestionAnswerTrue;
    [gameOver.view setFrame:CGRectMake(0, gameOver.view.frame.size.height, gameOver.view.frame.size.width, gameOver.view.frame.size.height)];
    [self.view addSubview:gameOver.view];
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionTransitionNone animations:^{
        [gameOver.view setFrame:CGRectMake(0, 20, gameOver.view.frame.size.width, gameOver.view.frame.size.height)];
    }completion:^(BOOL finished){
        //  reset game
        [_imageQuestion setImage:nil];
        [_imageQuestion setImage:[UIImage imageNamed:imageQuestionField.nameImage]];
        [self resetView];
        
        //  show question
        [_lblQuestion setHidden:NO];
        [_lblQuestion setFrame:CGRectMake(_lblQuestion.frame.origin.x, 25, _lblQuestion.frame.size.width, _lblQuestion.frame.size.height)];
    }];
}



/**
 * Blur Image
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (UIImage*) blur:(UIImage*)theImage
{
    // ***********If you need re-orienting (e.g. trying to blur a photo taken from the device camera front facing camera in portrait mode)
    // theImage = [self reOrientIfNeeded:theImage];
    
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:50.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
    
    // *************** if you need scaling
    // return [[self class] scaleIfNeeded:cgImage];
}


/********************************************************************************/


/********************************************************************************/
#pragma mark - Play Game Again



/**
 * Reset View
 *
 * @param param
 * @returns <#returns#>
 */
-(void)resetView
{
    [timer invalidate];
    [_timerAnwser setFrame:CGRectMake(0, _timerAnwser.frame.origin.y, _timerAnwser.frame.size.width, _timerAnwser.frame.size.height)];
    [_lblQuestion setFrame:CGRectMake(_lblQuestion.frame.origin.x, 310, _lblQuestion.frame.size.width, _lblQuestion.frame.size.height)];
    [_lblQuestion setHidden:YES];
    imageQuestionField = nil;
    imageNeedBlur = nil;
}


/**
 * Play again
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)playGameAgain
{
    //  score
    _lblScore.text = @"0";
    score = 0;
    [timer invalidate];
    
    //  get list image
    ImageQuestionModel *imageQuestionModel = [ImageQuestionModel new];
    listImageQuestion = [imageQuestionModel getAllRowsInTableImageQuestion];
    
    [_btnFalse setHidden:NO];
    [_btnTrue setHidden:NO];
    [_btn_StartGame setHidden:NO];
    [_imageQuestion setImage:[UIImage imageNamed:@"Questions1.jpg"]];
}



/**
 * Home Screen
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)popToHomeScreen
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


/**
 * Wait to start game
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_StartGame:(id)sender {
    //  start game
    //  view
    [self nextQuestion];
    [_btnFalse setHidden:NO];
    [_btnTrue setHidden:NO];
    
    [_btn_StartGame setHidden:YES];
    
    //  sound
    [soundBackground prepareToPlay];
    [soundBackground play];
}



/********************************************************************************/



/********************************************************************************/
#pragma mark - Sound




/**
 * initialize
 *
 * @param <#param#>
 * @returns <#returns#>
 */
-(void)initializeSound
{
    NSError *error;
    //  Sound score up
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"scored"
                                                              ofType:@"aiff"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    soundScoreUp = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
    [soundScoreUp prepareToPlay];
    
    
    //  Sound fail
    soundFilePath = [[NSBundle mainBundle] pathForResource:@"fail"
                                                    ofType:@"wav"];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    soundFail = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
    [soundFail prepareToPlay];
    
    
    //  Sound Background
    soundFilePath = [[NSBundle mainBundle] pathForResource:@"backgroundSound"
                                                    ofType:@"mp3"];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    soundBackground = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:&error];
    [soundBackground setNumberOfLoops:-1];
    [soundBackground setVolume:0.5];
    [soundBackground prepareToPlay];
}


/**
 * Turn on turn off sound
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_OnOffSound:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    //  mute
    if (![button isSelected]) {
        [button setImage:[UIImage imageNamed:@"sound_off_ipad"] forState:UIControlStateNormal];
        [soundBackground setVolume:0];
        [soundFail setVolume:0];
        [soundScoreUp setVolume:0];
        [button setSelected:YES];
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"sound_on_ipad"] forState:UIControlStateNormal];
        [soundBackground setVolume:0.5];
        [soundFail setVolume:1.0];
        [soundScoreUp setVolume:1.0];
        [button setSelected:NO];
    }
}

/********************************************************************************/


@end
