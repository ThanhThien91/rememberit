//
//  QuestionViewController.h
//  AirPrint
//
//  Created by Thien Thanh on 7/7/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import "ConfigApp.h"
#import "MPAdView.h"

@interface QuestionViewController : UIViewController<MPAdViewDelegate>

//  Outlet
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UIImageView *imageQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UIView *timerAnwser;
@property (weak, nonatomic) IBOutlet UIView *test;

//  Button Answer
@property (weak, nonatomic) IBOutlet UIButton *btnFalse;
@property (weak, nonatomic) IBOutlet UIButton *btnTrue;


//  Start Game
- (IBAction)action_StartGame:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_StartGame;

//  Sound
- (IBAction)action_OnOffSound:(id)sender;

//  Action
- (IBAction)action_SelectFalse:(id)sender;
- (IBAction)action_SelectTrue:(id)sender;
@end
