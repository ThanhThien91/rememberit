//
//  HomeViewController.h
//  AirPrint
//
//  Created by Thien Thanh on 7/8/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GameKit/GameKit.h>
#import "MPAdView.h"

@interface HomeViewController : UIViewController<GKGameCenterControllerDelegate,MPAdViewDelegate>



- (IBAction)action_PushToGameView:(id)sender;
- (IBAction)action_ShowLeaderboard:(id)sender;
- (IBAction)action_RateApp:(id)sender;

@end
