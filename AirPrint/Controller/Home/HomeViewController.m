//
//  HomeViewController.m
//  AirPrint
//
//  Created by Thien Thanh on 7/8/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import "HomeViewController.h"
#import "QuestionViewController.h"

@interface HomeViewController ()
{

}

@property (nonatomic, retain) MPAdView *adView;

// A flag indicating whether the Game Center features can be used after a user has been authenticated.
@property (nonatomic) BOOL gameCenterEnabled;

// It's used to display either the leaderboard or the achievements to the player.
-(void)showLeaderboard;

@property (nonatomic, strong) NSString *leaderboardIdentifier;


@end

@implementation HomeViewController


/********************************************************************************/
#pragma mark  ViewDidLoad



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/** Register notification center for view controller */
-(void) registerNotification
{
    
}


/** Set data when view did load.
 ** Be there. You can set up some variables, data, or any thing that have reletive to data type*/
-(void) setDataWhenViewDidLoad
{
    
}


/** Set view when view did load
 ** Be there. You can change the layout, view, button,..*/
-(void) setViewWhenViewDidLoad
{
    //  login gamecenter
    [self authenticateLocalPlayer];
    
    //  ads
    [self loadMopubAds];
    
}


/** Begin view controller */
- (void)viewDidLoad
{
    [self registerNotification];
    [self setViewWhenViewDidLoad];
    [self setDataWhenViewDidLoad];
    
    [super viewDidLoad];
}


-(void) viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}


/********************************************************************************/



/********************************************************************************/
#pragma mark - Mopub Ads
-(void)loadMopubAds {
    self.adView = [[MPAdView alloc] initWithAdUnitId:MOPUB_ADS_UNIT_BANNER
                                                 size:MOPUB_LEADERBOARD_SIZE];
    self.adView.delegate = self;
    CGRect frame = self.adView.frame;
    CGSize size = [self.adView adContentViewSize];
    frame.origin.y = [[UIScreen mainScreen] applicationFrame].size.height - size.height;
    self.adView.frame = frame;
    [self.view addSubview:self.adView];
    [self.adView loadAd];
}




/********************************************************************************/


#pragma mark - <MPAdViewDelegate>
- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/********************************************************************************/
#pragma mark - Game View


- (IBAction)action_PushToGameView:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    QuestionViewController *questionViewController = [storyboard instantiateViewControllerWithIdentifier:@"QuestionViewController"];
    [self.navigationController pushViewController:questionViewController animated:YES];
}



- (IBAction)action_RateApp:(id)sender {
}

/********************************************************************************/



/********************************************************************************/
#pragma mark - Leaderboard



/**
 * Show leaderboard
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_ShowLeaderboard:(id)sender {
    [self showLeaderboard];
}


-(void)showLeaderboard
{
    // Init the following view controller object.
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    // Set self as its delegate.
    gcViewController.gameCenterDelegate = self;
    
    // Depending on the parameter, show either the leaderboard or the achievements.
    gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
    gcViewController.leaderboardIdentifier = kLeaderboardIdentifier;
    // Finally present the view controller.
    [self presentViewController:gcViewController animated:YES completion:nil];

}



/********************************************************************************/


/********************************************************************************/
#pragma mark - GKGameCenterControllerDelegate method implementation


-(void)authenticateLocalPlayer{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil) {
            [self presentViewController:viewController animated:YES completion:nil];
        }
        else{
            if ([GKLocalPlayer localPlayer].authenticated) {
                _gameCenterEnabled = YES;
                
                // Get the default leaderboard identifier.
                [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                    
                    if (error != nil) {
                        NSLog(@"%@", [error localizedDescription]);
                    }
                    else{
                        _leaderboardIdentifier = kLeaderboardIdentifier;
                    }
                }];
            }
            
            else{
                _gameCenterEnabled = NO;
            }
        }
    };
}


-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}


/********************************************************************************/


@end
