//
//  GameOverViewController.h
//  AirPrint
//
//  Created by Thien Thanh on 7/8/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameOverViewController : UIViewController<UIPrintInteractionControllerDelegate>

//  Score
@property (weak, nonatomic) IBOutlet UILabel *lblNewScore;
@property (weak, nonatomic) IBOutlet UILabel *lblBestScore;
@property (weak, nonatomic) IBOutlet UIButton *btn_Print;

@property (strong, nonatomic) NSMutableArray *listImageQuestionAnswerTrue,*listQuestionOfImageAnswerTrue;
@property (assign, nonatomic) int score;

//  Menu
- (IBAction)action_PlayAgain:(id)sender;
- (IBAction)action_GoToHomeScreen:(id)sender;

//  Print
- (IBAction)action_PrintResult:(id)sender;

//  Social
- (IBAction)shareTwitter:(id)sender;
- (IBAction)shareFacebook:(id)sender;
@end
