//
//  GameOverViewController.m
//  AirPrint
//
//  Created by Thien Thanh on 7/8/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import "GameOverViewController.h"
#import "Model.h"
#import "HomeViewController.h"
#import <Social/Social.h>

@interface GameOverViewController ()
{
    HomeViewController *homeViewController;
}
@end

@implementation GameOverViewController


/********************************************************************************/
#pragma mark  ViewDidLoad


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/** Register notification center for view controller */
-(void) registerNotification
{
    
}


/** Set data when view did load.
 ** Be there. You can set up some variables, data, or any thing that have reletive to data type*/
-(void) setDataWhenViewDidLoad
{
    
}


/** Set view when view did load
 ** Be there. You can change the layout, view, button,..*/
-(void) setViewWhenViewDidLoad
{
    _lblNewScore.text = [NSString stringWithFormat:@"%i",_score];
    
    ScoreModel *scoreModel = [ScoreModel new];
    ScoreField *scoreField = [scoreModel getAllRowsInTableScore][0];
    _lblBestScore.text = [NSString stringWithFormat:@"%i",scoreField.bestScore];
    
    //  Update best score
    if (_score > scoreField.bestScore) {
        scoreField.bestScore = _score;
        [scoreModel updateRowInTableScore:scoreField];
    }

    //  report score
    [self reportScore];
}


/** Begin view controller */
- (void)viewDidLoad
{
    [self registerNotification];
    [self setViewWhenViewDidLoad];
    [self setDataWhenViewDidLoad];
    
    [super viewDidLoad];
}


-(void) viewWillAppear:(BOOL)animated
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}

/********************************************************************************/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/********************************************************************************/
#pragma mark - Action


/**
 * Play game again
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_PlayAgain:(id)sender {
    [self.view removeFromSuperview];
    
    // post NSNotification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayGameAgain" object:nil userInfo:nil];
}



/**
 * Home
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_GoToHomeScreen:(id)sender {
    // post NSNotification
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HomeScreen" object:nil userInfo:nil];
}



/**
 * Print Result
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)action_PrintResult:(id)sender {
    if ([UIPrintInteractionController isPrintingAvailable])
    {
        UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
        pic.delegate = self;
        
        
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = @"Dev";
        pic.printInfo = printInfo;
        
        
        // Load the html as a string from the file system
        NSString *path = [[NSBundle mainBundle] pathForResource:@"ResultPDF" ofType:@"html"];
        NSString *html = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        
        //  Score
        html = [html stringByReplacingOccurrencesOfString:@"scoreNumber" withString:[NSString stringWithFormat:@"%i",_score]];
        
        //  list question
        NSString *listImage = @"";
        for (int i = 0; i < [_listImageQuestionAnswerTrue count] - 1; i++) {
            ImageQuestionField *object = _listImageQuestionAnswerTrue[i];
            QuestionField *question = _listQuestionOfImageAnswerTrue[i];
            NSString *stringNeedReplace = @"<tr><td>&nbsp;</td><td style='text-align: center; height: 920px;vertical-align: top;'><p>QuestionHere</p><img alt='' src='imagehere' style='width: 728px; height: 487px;' /></td><td>&nbsp;</td></tr>";
            stringNeedReplace = [stringNeedReplace stringByReplacingOccurrencesOfString:@"imagehere" withString:[object nameImage]];
            stringNeedReplace = [stringNeedReplace stringByReplacingOccurrencesOfString:@"QuestionHere" withString:[NSString stringWithFormat:@"%@",question.question]];
            listImage = [listImage stringByAppendingString:stringNeedReplace];
        }
        
        
        //  Replace
        html = [html stringByReplacingOccurrencesOfString:@"AddRow" withString:listImage];
        
        // Tell the web view to load it
        UIWebView *webView = [UIWebView new];
        [webView loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
        UIViewPrintFormatter *formatter = [webView viewPrintFormatter];
        pic.printFormatter = formatter;
        pic.showsPageRange = NO;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            if (!completed && error) {
                NSLog(@"Printing could not complete because of error: %@", error);
            }
        };
        
        [pic presentFromRect:CGRectMake(_btn_Print.frame.origin.x, _btn_Print.frame.origin.y, _btn_Print.frame.size.width, _btn_Print.frame.size.height) inView:self.view animated:YES completionHandler:completionHandler];
        //[pic presentFromBarButtonItem:_btn_StartGame animated:YES completionHandler:completionHandler];
        //[pic presentAnimated:YES completionHandler:completionHandler];
    } else {
        // Not Available
    }
}





/********************************************************************************/



/********************************************************************************/
#pragma mark - Support Score To Leaderboard


-(void)reportScore{
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:kLeaderboardIdentifier];
        score.value = _score;
        
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error != nil) {
                NSLog(@"%@", [error localizedDescription]);
            }
        }];
    }
}


/********************************************************************************/



/********************************************************************************/
#pragma mark - Share Facebook


/**
 * Share Facebook
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)shareFacebook:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [controller setInitialText:[NSString stringWithFormat:@"Share Score : %li",(long)_score]];
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please go to Settings to login Facebook" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

}



/********************************************************************************/


/********************************************************************************/
#pragma mark - Twitter



/**
 * Share Twitter
 *
 * @param <#param#>
 * @returns <#returns#>
 */
- (IBAction)shareTwitter:(id)sender {
    //  Create an instance of the Tweet Sheet
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:
                                           SLServiceTypeTwitter];
    
    // Sets the completion handler.  Note that we don't know which thread the
    // block will be called on, so we need to ensure that any required UI
    // updates occur on the main queue
    tweetSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        switch(result) {
                //  This means the user cancelled without sending the Tweet
            case SLComposeViewControllerResultCancelled:
                break;
                //  This means the user hit 'Send'
            case SLComposeViewControllerResultDone:
                break;
        }
    };
    
    //  Set the initial body of the Tweet
    [tweetSheet setInitialText:[NSString stringWithFormat:@"Share Score : %li",(long)_score]];
    
    //  Adds an image to the Tweet.  For demo purposes, assume we have an
    //  image named 'larry.png' that we wish to attach
    if (![tweetSheet addImage:[UIImage imageNamed:@"larry.png"]]) {
        NSLog(@"Unable to add the image!");
    }
    
    //  Add an URL to the Tweet.  You can add multiple URLs.
    if (![tweetSheet addURL:[NSURL URLWithString:@"http://twitter.com/"]]){
        NSLog(@"Unable to add the URL!");
    }
    
    //  Presents the Tweet Sheet to the user
    [self presentViewController:tweetSheet animated:NO completion:^{
        NSLog(@"Tweet sheet has been presented.");
    }];
}


/********************************************************************************/





@end
