

////ImageQuestionField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-07-07 04:26:41 +0000. 
//

//--CREATE TABLE "ImageQuestion" ("nameImage" TEXT, "level" INTEGER, )

#import "ImageQuestionModel.h"

@implementation ImageQuestionModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"ImageQuestion";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableImageQuestion:(ImageQuestionField *) data
{
	NSString *nameImage = data.nameImage;
	int level = data.level;

	NSString *nameImageInit = @"";
	int levelInit = 0;


	if (nameImage)
		nameImageInit = nameImage;

	if (level)
		levelInit = level;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"ImageQuestion\" (\"nameImage\",\"level\") VALUES (?1,?2)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--nameImage
	sqlite3_bind_text(createStmt, 1, [nameImageInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--level
	sqlite3_bind_int(createStmt, 2, levelInit);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableImageQuestion:(ImageQuestionField *) data
{

	NSString *nameImage = data.nameImage;
	int level = data.level;

	NSString *nameImageInit = @"";
	int levelInit = 0;


	if (nameImage)
		nameImageInit = nameImage;

	if (level)
		levelInit = level;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"ImageQuestion\" SET \"nameImage\" = ?, \"level\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--nameImage
	sqlite3_bind_text(createStmt, 1, [nameImageInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--level
	sqlite3_bind_int(createStmt, 2, levelInit);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableImageQuestion
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--nameImage
			char *nameImage = (char *) sqlite3_column_text(statement, 1);
			NSString *nameImageStr = (nameImage == NULL) ? @"":[NSString stringWithUTF8String:nameImage];

			//--level
			char *level = (char *) sqlite3_column_text(statement, 2);
			NSString *levelStr = (level == NULL) ? @"":[NSString stringWithUTF8String:level];



			//--add Object
			ImageQuestionField *object = [[ImageQuestionField alloc]init];

			object.rowid = rowidStr;
			object.nameImage = nameImageStr;
			object.level = [levelStr isEqualToString: @""] ? 0 :[levelStr intValue];
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(ImageQuestionField *) getRowByRowsInTableImageQuestionByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_ImageQuestionField_BySQL:qsql];
}


//--Retrieving Data
-(ImageQuestionField *) getData_ImageQuestionField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	ImageQuestionField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--nameImage
			char *nameImage = (char *) sqlite3_column_text(statement, 1);
			NSString *nameImageStr = (nameImage == NULL) ? @"":[NSString stringWithUTF8String:nameImage];

			//--level
			char *level = (char *) sqlite3_column_text(statement, 2);
			NSString *levelStr = (level == NULL) ? @"":[NSString stringWithUTF8String:level];



			//--add Object
			object = [[[ImageQuestionField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.nameImage = nameImageStr;
			object.level = [levelStr isEqualToString: @""] ? 0 :[levelStr intValue];
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end