

////ImageQuestionModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-07-07 04:26:41 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "ImageQuestionField.h"

@interface ImageQuestionModel : AppModel
{

}


/** Get all rows in Table ImageQuestion **/
-(NSMutableArray *) getAllRowsInTableImageQuestion;

/** Get rows by rowid in Table ImageQuestion **/
-(ImageQuestionField *) getRowByRowsInTableImageQuestionByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableImageQuestion:(ImageQuestionField *) data;

/** update row **/
-(BOOL)updateRowInTableImageQuestion:(ImageQuestionField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end