

////ScoreModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-07-08 04:31:10 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "ScoreField.h"

@interface ScoreModel : AppModel
{

}


/** Get all rows in Table Score **/
-(NSMutableArray *) getAllRowsInTableScore;

/** Get rows by rowid in Table Score **/
-(ScoreField *) getRowByRowsInTableScoreByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableScore:(ScoreField *) data;

/** update row **/
-(BOOL)updateRowInTableScore:(ScoreField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end