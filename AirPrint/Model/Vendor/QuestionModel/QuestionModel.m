

////QuestionField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-07-07 06:40:07 +0000. 
//

//--CREATE TABLE "Question" ("question" TEXT, "answer" INTEGER, "idImage" TEXT, )

#import "QuestionModel.h"

@implementation QuestionModel

/** init class */
-(id) init
{
	self = [super init];

	if (self)
	{
		self.usesTable = @"Question";
	}
	return self;
}


/* free memory */
-(void) dealloc
{
	[super dealloc];
}



/********************************************************************************/
#pragma mark  - Insert Row To Table

-(BOOL)createRowInTableQuestion:(QuestionField *) data
{
	NSString *question = data.question;
	int answer = data.answer;
	NSString *idImage = data.idImage;

	NSString *questionInit = @"";
	int answerInit = 0;
	NSString *idImageInit = @"";


	if (question)
		questionInit = question;

	if (answer)
		answerInit = answer;

	if (idImage)
		idImageInit = idImage;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		const char *sql = "INSERT INTO \"Question\" (\"question\",\"answer\",\"idImage\") VALUES (?1,?2,?3)";

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--question
	sqlite3_bind_text(createStmt, 1, [questionInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--answer
	sqlite3_bind_int(createStmt, 2, answerInit);

 	//--idImage
	sqlite3_bind_text(createStmt, 3, [idImageInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Update Row To Table

-(BOOL)updateRowInTableQuestion:(QuestionField *) data
{

	NSString *question = data.question;
	int answer = data.answer;
	NSString *idImage = data.idImage;

	NSString *questionInit = @"";
	int answerInit = 0;
	NSString *idImageInit = @"";


	if (question)
		questionInit = question;

	if (answer)
		answerInit = answer;

	if (idImage)
		idImageInit = idImage;

	sqlite3_stmt *createStmt = nil;

	if (createStmt == nil)
	{
		//--string sql
		NSString *sqlOriginal = [NSString stringWithFormat:@"UPDATE \"Question\" SET \"question\" = ?, \"answer\" = ?, \"idImage\" = ? WHERE rowid = %@", data.rowid];

		const char *sql = sqlOriginal.UTF8String;

		if(sqlite3_prepare_v2(self.db, sql, -1, &createStmt, NULL) != SQLITE_OK)
			NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(self.db));
	}

 	//--question
	sqlite3_bind_text(createStmt, 1, [questionInit UTF8String], -1, SQLITE_TRANSIENT);

 	//--answer
	sqlite3_bind_int(createStmt, 2, answerInit);

 	//--idImage
	sqlite3_bind_text(createStmt, 3, [idImageInit UTF8String], -1, SQLITE_TRANSIENT);



	if(SQLITE_DONE != sqlite3_step(createStmt))
	{
		//NSLog(@"Error code: %d", sqlite3_step(createStmt));
		sqlite3_close(db);
		NSAssert1(0, @"Error while inserting data. '%d'", sqlite3_step(createStmt));
		return NO;
	}

	//--NSLog(@"id isnert: %llu", sqlite3_last_insert_rowid(db));
	sqlite3_finalize(createStmt);

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Delete Row To Table

-(BOOL)deleteRowByRowId:(NSString *) rowid
{
	NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE rowid = %@", self.usesTable, rowid];
	//--NSLog(@"sql: %@", sql);

	char *err;
	if (sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {

		NSLog(@"reture code: %d",sqlite3_exec(db, [sql UTF8String], NULL, NULL, &err));

		sqlite3_close(db);
		NSAssert(0, @"Error insert table ZCARDGAME.");

	}

	return YES;
}

/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select All Row In Table

-(NSMutableArray *) getAllRowsInTableQuestion
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ ", self.usesTable];

	return [self getArrayDataBy_SQL:qsql];
}


//--Retrieving Array Data
-(NSMutableArray *) getArrayDataBy_SQL:(NSString *)qsql
{
	NSMutableArray *array = [NSMutableArray array];

	sqlite3_stmt *statement;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--question
			char *question = (char *) sqlite3_column_text(statement, 1);
			NSString *questionStr = (question == NULL) ? @"":[NSString stringWithUTF8String:question];

			//--answer
			char *answer = (char *) sqlite3_column_text(statement, 2);
			NSString *answerStr = (answer == NULL) ? @"":[NSString stringWithUTF8String:answer];

			//--idImage
			char *idImage = (char *) sqlite3_column_text(statement, 3);
			NSString *idImageStr = (idImage == NULL) ? @"":[NSString stringWithUTF8String:idImage];



			//--add Object
			QuestionField *object = [[QuestionField alloc]init];

			object.rowid = rowidStr;
			object.question = questionStr;
			object.answer = [answerStr isEqualToString: @""] ? 0 :[answerStr intValue];
			object.idImage = idImageStr;
			
			[array addObject:object];

			//--free memory
			[object release];

		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return array;
}



/********************************************************************************/



/********************************************************************************/
#pragma mark - Select row by idImage in table


-(NSMutableArray *) getRowByRowsInTableQuestionByIdImage:(NSString *) idImage
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE idImage = \"%@\" ", self.usesTable,idImage];
    
	return [self getArrayDataBy_SQL:qsql];
}



/********************************************************************************/


/********************************************************************************/
#pragma mark  - Select Row By rowid In Table

-(QuestionField *) getRowByRowsInTableQuestionByRowId:(NSString *) rowid
{
	//--retrive rows
	NSString *qsql = [NSString stringWithFormat:@"SELECT rowid,* FROM %@ WHERE rowid = \"%@\" ", self.usesTable,rowid];

	return [self getData_QuestionField_BySQL:qsql];
}



//--Retrieving Data
-(QuestionField *) getData_QuestionField_BySQL:(NSString *) qsql
{
	sqlite3_stmt *statement;
	QuestionField *object = nil;

	if (sqlite3_prepare_v2(self.db, [qsql UTF8String], -1, &statement, nil) == SQLITE_OK)
	{
		while(sqlite3_step(statement) == SQLITE_ROW)
		{
			//--rowid
			char *rowid = (char *) sqlite3_column_text(statement, 0);
			NSString *rowidStr  = [NSString stringWithUTF8String:rowid];

			//--question
			char *question = (char *) sqlite3_column_text(statement, 1);
			NSString *questionStr = (question == NULL) ? @"":[NSString stringWithUTF8String:question];

			//--answer
			char *answer = (char *) sqlite3_column_text(statement, 2);
			NSString *answerStr = (answer == NULL) ? @"":[NSString stringWithUTF8String:answer];

			//--idImage
			char *idImage = (char *) sqlite3_column_text(statement, 3);
			NSString *idImageStr = (idImage == NULL) ? @"":[NSString stringWithUTF8String:idImage];



			//--add Object
			object = [[[QuestionField alloc] init] autorelease];

			object.rowid = rowidStr;
			object.question = questionStr;
			object.answer = [answerStr isEqualToString: @""] ? 0 :[answerStr intValue];
			object.idImage = idImageStr;
			
		}
	}

	if (statement)
	{
		sqlite3_reset(statement);
		sqlite3_finalize(statement);
	}

	return object;
}



/********************************************************************************/
@end