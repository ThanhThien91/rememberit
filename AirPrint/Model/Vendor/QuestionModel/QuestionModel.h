

////QuestionModel.h 
//  Project 
// 
//  Created by TeamiOS on 2014-07-07 06:40:07 +0000. 
//
// 

#import <Foundation/Foundation.h>
#import "AppModel.h"
#import "QuestionField.h"

@interface QuestionModel : AppModel
{

}


/** Get all rows in Table Question **/
-(NSMutableArray *) getAllRowsInTableQuestion;

-(NSMutableArray *) getRowByRowsInTableQuestionByIdImage:(NSString *) idImage;


/** Get rows by rowid in Table Question **/
-(QuestionField *) getRowByRowsInTableQuestionByRowId:(NSString *) rowid;


/** create a new row **/
-(BOOL)createRowInTableQuestion:(QuestionField *) data;

/** update row **/
-(BOOL)updateRowInTableQuestion:(QuestionField *) data;

/** delete row **/
-(BOOL)deleteRowByRowId:(NSString *) rowid;

@end