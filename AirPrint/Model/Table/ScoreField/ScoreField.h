


////ScoreField.h 
//  Project 
// 
// Created by TeamiOS on 2014-07-08 04:31:10 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface ScoreField : NSObject
{
	NSString *rowid;
	int bestScore;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, assign) int bestScore;
 

@end