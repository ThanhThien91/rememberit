


////QuestionField.h 
//  Project 
// 
// Created by TeamiOS on 2014-07-07 06:40:07 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface QuestionField : NSObject
{
	NSString *rowid;
	NSString *question;
	int answer;
	NSString *idImage;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *question;
@property (nonatomic, assign) int answer;
@property (nonatomic, retain) NSString *idImage;
 

@end