

////QuestionField.m 
//  Project 
// 
//  Created by TeamiOS on 2014-07-07 06:40:07 +0000. 
//
// 

#import "QuestionField.h"

@implementation QuestionField
@synthesize rowid;
@synthesize question;
@synthesize answer;
@synthesize idImage;


-(void) dealloc
{
	[rowid release];
	[question  release];
	[idImage  release];

	[super  dealloc];
}

@end