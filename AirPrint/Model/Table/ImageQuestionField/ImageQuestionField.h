


////ImageQuestionField.h 
//  Project 
// 
// Created by TeamiOS on 2014-07-07 04:26:41 +0000. 
//
// 

#import <Foundation/Foundation.h>

@interface ImageQuestionField : NSObject
{
	NSString *rowid;
	NSString *nameImage;
	int level;
}

@property (nonatomic, retain) NSString *rowid;
@property (nonatomic, retain) NSString *nameImage;
@property (nonatomic, assign) int level;
 

@end