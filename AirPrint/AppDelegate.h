//
//  AppDelegate.h
//  AirPrint
//
//  Created by Thien Thanh on 7/7/14.
//  Copyright (c) 2014 Kabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL isTutorial;
@property (strong, nonatomic) NSString *databasePath;


@end
